import 'jest-canvas-mock';
import 'jest-preset-angular/setup-jest';

Object.defineProperty(window, 'ResizeObserver', {
  value: class ResizeObserver {
    disconnect: jest.Mock = jest.fn();

    observe: jest.Mock = jest.fn();

    unobserve: jest.Mock = jest.fn();
  },
});

Object.defineProperty(window, 'IntersectionObserver', {
  value: class IntersectionObserver {
    disconnect: jest.Mock = jest.fn();

    observe: jest.Mock = jest.fn();

    unobserve: jest.Mock = jest.fn();
  },
});

const { getComputedStyle }: typeof window = window;
window.getComputedStyle = (elt: Element) => getComputedStyle(elt);

Object.defineProperty(window, 'setImmediate', {
  value: jest.useRealTimers,
});
