import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataWorkerService } from "./modules/data/workers/data.worker.service";
import { DataService } from "./modules/data/services/data.service";
import { StorageService } from "./core/storages/storage.service";
import { DEFAULT_INTERVAL, DEFAULT_NUMBER_OF_ARRAYS } from "./core/constants/app.constants";
import { DashboardWorkerService } from "./modules/dashboard/workers/dashboard-worker.service";
import { NgForTrackByIdDirective } from './core/directives/ng-for-track-by-id.directive';

@NgModule({
  declarations: [
    AppComponent,
    NgForTrackByIdDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (
        dataWorkerService: DataWorkerService,
        dataService: DataService,
        dashboardWorkerService: DashboardWorkerService,
        storageService: StorageService
      ) => () => {
        dataWorkerService.init();
        dataService.init();
        dashboardWorkerService.init();
        storageService.setData({
          interval: DEFAULT_INTERVAL,
          numberOfArrays: DEFAULT_NUMBER_OF_ARRAYS,
        });
      },
      deps: [DataWorkerService, DataService, DashboardWorkerService, StorageService],
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
