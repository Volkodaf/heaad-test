/* eslint-disable @typescript-eslint/no-explicit-any */
type Func = (...args: any[]) => any;

export type SpyObject<T> = T & {
  [K in keyof T]: T[K] extends Func ? T[K] & ReturnType<typeof jest.fn> : T[K];
};

export function createSpyObject<T>(
  methodNames: ReadonlyArray<keyof T>,
  properties: Record<string, unknown> = {},
): SpyObject<T> {
  const obj: any = {};

  methodNames.forEach((methodName: keyof T) => {
    obj[methodName] = jest.fn();
  });

  return Object.assign(obj, properties);
}
