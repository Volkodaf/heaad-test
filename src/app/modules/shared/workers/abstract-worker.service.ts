import { DashboardItem } from "../../dashboard/types/dashboard.types";

/**
 * Some methods moved to the abstract class to not duplicate same functions
 */
export default abstract class AbstractWorkerService {
  protected worker: Worker|undefined;

  /**
   * Post message
   *
   * @param message
   */
  postMessage<T>(message: T): void {
    this.worker?.postMessage(message);
  }

  /**
   * Init worker
   *
   * @protected
   */
  init() {
    if (typeof Worker !== 'undefined') {
      this.createWorkerInstance();
      if (this.worker) {
        this.worker.onmessage = ({ data }) => {
          this.processMessage(data);
        };
      }
    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
      console.error('Web workers don\'t available');
    }
  }

  /**
   * Create worker instance
   * Abstract - because every particular workerService must provide its own string path when create new instance
   *
   * @protected
   */
  protected abstract createWorkerInstance(): void;

  /**
   * Process worker message
   *
   * @param data
   * @protected
   */
  protected abstract processMessage(data: DashboardItem[]): void;
}
