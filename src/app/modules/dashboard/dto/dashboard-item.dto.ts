import { DashboardItem } from "../types/dashboard.types";
import { Color } from "../../../core/constants/app.constants";
import DashboardItemChildDto from "./dashboard-item-child.dto";

export default class DashboardItemDto {
  private readonly _id: string;
  private readonly _int: number;
  private readonly _float: number;
  private readonly _color: Color;
  private readonly _child: DashboardItemChildDto;

  constructor(dashboardItem: DashboardItem) {
    this._id = dashboardItem.id;
    this._int = dashboardItem.int;
    this._float = dashboardItem.float;
    this._color = dashboardItem.color;
    this._child = new DashboardItemChildDto(dashboardItem.child);
  }

  get id(): string {
    return this._id;
  }

  get int(): number {
    return this._int;
  }

  get float(): number {
    return this._float;
  }

  get color(): Color {
    return this._color;
  }

  get child(): DashboardItemChildDto {
    return this._child;
  }
}
