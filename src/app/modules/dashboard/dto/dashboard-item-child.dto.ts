import { Color } from "../../../core/constants/app.constants";
import { DashboardItemChild } from "../types/dashboard.types";

export default class DashboardItemChildDto {
  private readonly _id: string;
  private readonly _color: Color;

  constructor(dashboardChild: DashboardItemChild) {
    this._id = dashboardChild.id;
    this._color = dashboardChild.color;
  }

  get id(): string {
    return this._id;
  }

  get color(): Color {
    return this._color;
  }
}
