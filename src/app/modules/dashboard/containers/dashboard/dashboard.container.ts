import { Component } from '@angular/core';
import { StorageService } from "../../../../core/storages/storage.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.container.html',
  styleUrls: []
})
export class DashboardContainer {
  constructor(
    public storageService: StorageService,
  ) {}

  /**
   * Handle interval change
   *
   * @param interval
   */
  handleIntervalChange(interval: string) {
    this.storageService.setData({
      interval: this.prepareNumber(interval),
    });
  }

  /**
   * Handle number of arrays change
   *
   * @param numberOfArrays
   */
  handleNumberOfArraysChange(numberOfArrays: string) {
    this.storageService.setData({
      numberOfArrays: this.prepareNumber(numberOfArrays),
    });
  }

  /**
   * Handle Ids change
   *
   * @param ids
   */
  handleIdsChange(ids: string|null) {
    this.storageService.setData({
      additionalIds: this.prepareIds(ids),
    });
  }

  /**
   * Prepare number
   *
   * @param val
   */
  private prepareNumber(val: string): number {
    const valueAsNumber = Number(val);
    if (isNaN(valueAsNumber)) {
      return 0;
    }
    return valueAsNumber;
  }

  /**
   * Preparing ids before set to storage
   *
   * @param ids
   */
  private prepareIds(ids: string|null): string[] {
    if (!ids) {
      return [];
    }
    return ids.split(',').map(id => id.replace(/\s/g, '')).filter(id => !!id);
  }
}
