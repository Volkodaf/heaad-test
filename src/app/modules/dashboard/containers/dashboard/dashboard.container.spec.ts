import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardContainer } from './dashboard.container';
import { createSpyObject, SpyObject } from "../../../shared/utils/test.utils";
import { StorageService } from "../../../../core/storages/storage.service";

describe('DashboardComponent', () => {
  let component: DashboardContainer;
  let fixture: ComponentFixture<DashboardContainer>;
  let mockStorageService: SpyObject<StorageService>;

  beforeAll(() => {
    mockStorageService = createSpyObject<StorageService>(['setData']);
  })

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: StorageService,
          useValue: mockStorageService,
        }
      ],
      declarations: [ DashboardContainer ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle interval change', () => {
    component.handleIntervalChange('123');

    expect(mockStorageService.setData).toHaveBeenCalledWith({ interval: 123 });
  });

  it('should handle ids change', () => {
    component.handleIdsChange('123, 144');

    expect(mockStorageService.setData).toHaveBeenCalledWith({ additionalIds: ["123", "144"] });
  });

  it('should handle number of arrays change', () => {
    component.handleNumberOfArraysChange("10");

    expect(mockStorageService.setData).toHaveBeenCalledWith({ numberOfArrays: 10 });
  });
});
