import { Color } from "../../../core/constants/app.constants";

export type DashboardItemChild = {
  id: string;
  color: Color;
}

export type DashboardItem = {
  id: string;
  int: number;
  float: number;
  color: Color;
  child: DashboardItemChild;
}

export type DashboardWorkerRequest = {
  items: DashboardItem[],
  ids: string[]
}
