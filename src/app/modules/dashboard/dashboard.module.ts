import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardContainer } from './containers/dashboard/dashboard.container';
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardInputComponent } from './components/dashboard-input/dashboard-input.component';
import { DashboardNestedTableComponent } from './components/dashboard-nested-table/dashboard-nested-table.component';
import { DashboardTableComponent } from './components/dashboard-table/dashboard-table.component';
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    DashboardContainer,
    DashboardInputComponent,
    DashboardNestedTableComponent,
    DashboardTableComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule
  ]
})
export class DashboardModule { }
