import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardNestedTableComponent } from './dashboard-nested-table.component';

describe('DashboardNestedTableComponent', () => {
  let component: DashboardNestedTableComponent;
  let fixture: ComponentFixture<DashboardNestedTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardNestedTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardNestedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
