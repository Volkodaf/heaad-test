import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import DashboardItemChildDto from "../../dto/dashboard-item-child.dto";

@Component({
  selector: 'app-dashboard-nested-table',
  templateUrl: './dashboard-nested-table.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardNestedTableComponent {
  @Input() item?: DashboardItemChildDto;
}
