import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import DashboardItemDto from "../../dto/dashboard-item.dto";
import { Color } from "../../../../core/constants/app.constants";

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardTableComponent {
  public Color = Color;

  @Input() items: DashboardItemDto[] = [];
}
