import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-dashboard-input',
  templateUrl: './dashboard-input.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardInputComponent {
  @Input() label?: string;
  @Input() model?: string|number;

  @Output() output: EventEmitter<string> = new EventEmitter();

  handleChange() {
    this.output.emit(String(this.model));
  }
}
