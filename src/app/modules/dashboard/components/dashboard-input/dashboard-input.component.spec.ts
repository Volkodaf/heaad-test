import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardInputComponent } from './dashboard-input.component';
import { FormsModule } from "@angular/forms";

describe('DashboardInputComponent', () => {
  let component: DashboardInputComponent;
  let fixture: ComponentFixture<DashboardInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardInputComponent ],
      imports: [FormsModule],
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit result', () => {
    jest.spyOn(component.output, 'emit');
    component.handleChange();
    expect(component.output.emit).toHaveBeenCalled();
  });
});
