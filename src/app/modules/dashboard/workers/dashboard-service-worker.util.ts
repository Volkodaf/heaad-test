export function createDashboardWorker() {
  return new Worker(new URL('./dashboard.worker', import.meta.url));
}
