import { Injectable } from '@angular/core';
import AbstractWorkerService from "../../shared/workers/abstract-worker.service";
import { DashboardItem, DashboardWorkerRequest } from "../types/dashboard.types";
import { StorageService } from "../../../core/storages/storage.service";
import DashboardItemDto from "../dto/dashboard-item.dto";
import { map, Subscription, tap } from "rxjs";
import { createDashboardWorker } from "./dashboard-service-worker.util";

@Injectable({
  providedIn: 'root'
})
export class DashboardWorkerService extends AbstractWorkerService {
  private ids: string[] = [];
  private subscription: Subscription = this.storageService.storage$.pipe(
    map(item => item.additionalIds),
    tap(ids => this.ids = ids),
  ).subscribe();

  constructor(
    private storageService: StorageService
  ) {
    super();
  }

  /**
   * Unused method, should be called to remove subscription
   */
  destroy() {
    this.subscription.unsubscribe();
  }

  /**
   * Post message with overriding ids
   *
   * @param message
   */
  override postMessage<T>(message: T) {
    const typedMessage = <DashboardWorkerRequest>message;
    if (typedMessage.ids.length) {
      super.postMessage(message);
    } else {
      super.postMessage({
        ...message,
        ids: this.ids,
      });
    }
  }

  /**
   * Create worker instance
   *
   * @protected
   */
  protected createWorkerInstance() {
    this.worker = createDashboardWorker();
  }

  /**
   * Process worker message
   *
   * @param dashboardItems
   * @protected
   */
  protected processMessage(dashboardItems: DashboardItem[]): void {
    this.storageService.setData({
      data: this.transformToObjects(dashboardItems),
    });
  }

  /**
   * Transform to array of objects
   *
   * @param dashboardItems
   * @private
   */
  private transformToObjects(dashboardItems: DashboardItem[]): DashboardItemDto[] {
    return dashboardItems.map(item => new DashboardItemDto(item));
  }
}
