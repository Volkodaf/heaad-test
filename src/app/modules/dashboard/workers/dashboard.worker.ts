/// <reference lib="webworker" />

import { DASHBOARD_ITEMS_LENGTH } from "../constants/dashboard.constants";

addEventListener('message', ({ data }) => {
  const items = data.items.slice(-DASHBOARD_ITEMS_LENGTH);
  if (data.ids.length) {
    for (let i=0; i<data.ids.length; i++) {
      if (items[i]) {
        items[i].id = data.ids[i];
      }
    }
  }
  postMessage(items);
});
