import { TestBed } from '@angular/core/testing';
import { DashboardWorkerService } from "./dashboard-worker.service";
import { StorageService } from "../../../core/storages/storage.service";
import { of } from "rxjs";
import { createSpyObject } from "../../shared/utils/test.utils";

describe('WebWorkerService', () => {
  let service: DashboardWorkerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: StorageService,
          useValue: {
            storage$: of({ interval: 10, numberOfArrays: 100, additionalIds: [] })
          },
        }
      ]
    });
    service = TestBed.inject(DashboardWorkerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be inited, but without worker', () => {
    console.error = jest.fn();
    service.init();
    expect(console.error).toHaveBeenCalled();
  });

  it('should be destroyed', () => {
    service.destroy();
    expect(service['subscription'].closed).toBeTruthy();
  });

  it('should post message without ids', () => {
    service['worker'] = createSpyObject<Worker>(['postMessage']);
    service.postMessage({ ids: [] });

    expect(service['worker']?.postMessage).toHaveBeenCalled();
  });

  it('should post message with ids', () => {
    service['worker'] = createSpyObject<Worker>(['postMessage']);
    service.postMessage({ ids: ['123', '213'] });

    expect(service['worker']?.postMessage).toHaveBeenCalledWith({ ids: ['123', '213'] });
  });
});
