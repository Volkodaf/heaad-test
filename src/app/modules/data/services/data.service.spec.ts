import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import { StorageService } from "../../../core/storages/storage.service";
import { of } from "rxjs";
import { DataWorkerService } from "../workers/data.worker.service";

describe('DataService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: StorageService,
          useValue: {
            storage$: of({ interval: 10, numberOfArrays: 100, additionalIds: [] })
          },
        },
        {
          provide: DataWorkerService,
          useValue: {}
        }
      ]
    });
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be inited', () => {
    service.init();

    expect(typeof service['subscription']).toEqual('object');
  });
});
