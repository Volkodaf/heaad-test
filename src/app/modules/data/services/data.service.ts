import { Injectable } from '@angular/core';
import { DataWorkerService } from "../workers/data.worker.service";
import { StorageService } from "../../../core/storages/storage.service";
import { Subscription, tap } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private intervalId = -1;
  private subscription?: Subscription;

  constructor(
    private dataWorkerService: DataWorkerService,
    private storageService: StorageService,
  ) {}

  /**
   * Init
   */
  init() {
    this.subscription = this.storageService.storage$.pipe(
      tap(item => this.run(item.interval, item.numberOfArrays))
    ).subscribe();
  }

  /**
   * Run data creation
   *
   * @param interval
   * @param numberOfArrays
   */
  run(interval: number, numberOfArrays: number) {
    clearInterval(this.intervalId);
    if (interval > 0) {
      this.intervalId = +setInterval(() => {
        this.dataWorkerService.postMessage({
          numberOfArrays,
        });
      }, interval);
    }
  }

  /**
   * This is unused method, created for case if we don't need data creation loop anymore
   */
  destroy() {
    clearInterval(this.intervalId);
    this.subscription?.unsubscribe();
  }
}
