import { DashboardItem, DashboardItemChild } from "../../dashboard/types/dashboard.types";
import { Color } from "../../../core/constants/app.constants";
import random from 'random';

export class DataGeneratorService {
  run(numberOfArrays = 100): DashboardItem[] {
    const result = [];
    for (let i=1; i<=numberOfArrays; i++) {
      const dashboardItem: DashboardItem = {
        id: String(i),
        int: this.getInt(),
        float: this.getFloat(),
        color: this.getColor(),
        child: this.getChild(numberOfArrays),
      }
      result.push(dashboardItem);
    }
    return result;
  }

  /**
   * Get int
   *
   * @private
   */
  private getInt(): number {
    return random.int(0, 1000);
  }

  /**
   * Get float
   *
   * @private
   */
  private getFloat(): number {
    return Number(random.float().toPrecision(18));
  }

  /**
   * Get color
   *
   * @private
   */
  private getColor(): Color {
    return <Color>random.choice<Color>(Object.values(Color));
  }

  /**
   * Get child
   *
   * @param numberOfArrays
   * @private
   */
  private getChild(numberOfArrays: number): DashboardItemChild {
    return {
      id: String(random.int(0, numberOfArrays)),
      color: this.getColor(),
    }
  }
}
