import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * This module created just to implement logic for data generation.
 * If we have real BE service with socket.io - we don't need this module at all.
 * And if we will not have more workers than dashboard.worker - AbstractWorkerService we don't need too
 */
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class DataModule { }
