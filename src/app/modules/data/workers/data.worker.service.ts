import { Injectable } from '@angular/core';
import AbstractWorkerService from "../../shared/workers/abstract-worker.service";
import { SocketService } from "../../../core/services/socket.service";
import { DashboardItem } from "../../dashboard/types/dashboard.types";
import { createDataWorker } from "./data-service-worker.util";

@Injectable({
  providedIn: 'root'
})
export class DataWorkerService extends AbstractWorkerService {
  constructor(private socketService: SocketService) {
    super();
  }

  /**
   * Create worker instance
   *
   * @protected
   */
  protected createWorkerInstance() {
    this.worker = createDataWorker();
  }

  /**
   * Process message
   *
   * @param dashboardItems
   * @protected
   */
  protected processMessage(dashboardItems: DashboardItem[]) {
    this.socketService.onDashboardItemsReceived(dashboardItems);
  }
}
