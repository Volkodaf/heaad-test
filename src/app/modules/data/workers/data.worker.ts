/// <reference lib="webworker" />

import { DataGeneratorService } from "../services/data.generator.service";

addEventListener('message', ({ data }) => {
  const dataGenerator = new DataGeneratorService();
  const arrays = dataGenerator.run(data.numberOfArrays);
  postMessage(arrays);
});
