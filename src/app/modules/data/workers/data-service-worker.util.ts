export function createDataWorker() {
  return new Worker(new URL('./data.worker', import.meta.url));
}
