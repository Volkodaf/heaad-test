import { TestBed } from '@angular/core/testing';
import { StorageService } from "../../../core/storages/storage.service";
import { of } from "rxjs";
import { createSpyObject } from "../../shared/utils/test.utils";
import { DataWorkerService } from "./data.worker.service";

describe('WebWorkerService', () => {
  let service: DataWorkerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: StorageService,
          useValue: {
            storage$: of({ interval: 10, numberOfArrays: 100, additionalIds: [] })
          },
        }
      ]
    });
    service = TestBed.inject(DataWorkerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be inited, but without worker', () => {
    console.error = jest.fn();
    service.init();
    expect(console.error).toHaveBeenCalled();
  });

  it('should post message without ids', () => {
    service['worker'] = createSpyObject<Worker>(['postMessage']);
    service.postMessage({ ids: [] });

    expect(service['worker']?.postMessage).toHaveBeenCalled();
  });

  it('should post message with ids', () => {
    service['worker'] = createSpyObject<Worker>(['postMessage']);
    service.postMessage({ ids: ['123', '213'] });

    expect(service['worker']?.postMessage).toHaveBeenCalledWith({ ids: ['123', '213'] });
  });
});
