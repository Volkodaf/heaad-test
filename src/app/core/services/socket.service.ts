import { Injectable } from '@angular/core';
import { DashboardItem, DashboardWorkerRequest } from "../../modules/dashboard/types/dashboard.types";
import { DashboardWorkerService } from "../../modules/dashboard/workers/dashboard-worker.service";

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  constructor(
    private dashboardWorkerService: DashboardWorkerService
  ) { }

  // Idea, how it should work
  // socket.on('message', this.onDashboardItemsReceived.bind(this))

  /**
   * When we received socket event
   *
   * @param dashboardItems
   */
  onDashboardItemsReceived(dashboardItems: DashboardItem[]): void {
    this.dashboardWorkerService.postMessage<DashboardWorkerRequest>({
      items: dashboardItems,
      ids: []
    });
  }
}
