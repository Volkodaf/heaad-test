import { TestBed } from '@angular/core/testing';

import { SocketService } from './socket.service';
import { DashboardWorkerService } from "../../modules/dashboard/workers/dashboard-worker.service";
import { createSpyObject, SpyObject } from "../../modules/shared/utils/test.utils";

describe('SocketService', () => {
  let service: SocketService;
  let mockDashboardWorkerService: SpyObject<DashboardWorkerService>;

  beforeAll(() => {
    mockDashboardWorkerService = createSpyObject<DashboardWorkerService>(['postMessage']);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: DashboardWorkerService,
          useValue: mockDashboardWorkerService
        }
      ],
    });
    service = TestBed.inject(SocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should post message', () => {
    service.onDashboardItemsReceived([]);
    expect(mockDashboardWorkerService.postMessage).toHaveBeenCalled();
  });
});
