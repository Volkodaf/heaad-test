import { NgForOf } from '@angular/common';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgForTrackByIdDirective } from "./ng-for-track-by-id.directive";
import { createSpyObject, SpyObject } from "../../modules/shared/utils/test.utils";

@Component({
  template: '<p></p>',
})
class TestComponent {}

describe('CommonNgForTrackByIdDirective', () => {
  type NgForInstanceType = Required<{ id: string }>;

  let directive: NgForTrackByIdDirective<NgForInstanceType>;
  let fixture: ComponentFixture<TestComponent>;
  let mockNgFor: SpyObject<NgForOf<NgForInstanceType>>;

  beforeEach(() => {
    mockNgFor = createSpyObject<NgForOf<NgForInstanceType>>(['ngForTrackBy']);

    TestBed.configureTestingModule({
      declarations: [NgForTrackByIdDirective, TestComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();

    directive = new NgForTrackByIdDirective(mockNgFor);
  });

  it('should create', () => {
    expect(directive).toBeTruthy();
  });
});
