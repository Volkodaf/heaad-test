import { Directive, Host } from '@angular/core';
import { NgForOf } from "@angular/common";

interface TrackItem {
  id: string;
}

/**
 * Custom trackBy function takes item and return unique identifier for it
 * We cannot guarantee that ids will be unique, that's why concatenate them with indexes
 *
 * @example: `<div *ngFor="let item of items; trackById">`
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[ngForTrackById]'
})
export class NgForTrackByIdDirective<T extends TrackItem> {
  constructor(@Host() private ngFor: NgForOf<T>) {
    ngFor.ngForTrackBy = (index: number, item: T) => index + item.id;
  }
}
