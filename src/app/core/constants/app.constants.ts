// Created as enum for more comfortable usage if will need
export enum Color {
  RED = 'red',
  GREEN = 'green',
  YELLOW = 'yellow',
  BLUE = 'blue'
}

export const DEFAULT_INTERVAL = 1000;
export const DEFAULT_NUMBER_OF_ARRAYS = 100;
