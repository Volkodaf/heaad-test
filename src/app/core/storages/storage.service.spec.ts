import { fakeAsync, TestBed } from '@angular/core/testing';
import { StorageService } from "./storage.service";

describe('StorageService', () => {
  let service: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should update store', fakeAsync(() => {
    service.storage$.subscribe(item => {
      expect(item).toMatchObject({ data: [], numberOfArrays: 100, interval: 1000, additionalIds: [] });
    });
  }));
});
