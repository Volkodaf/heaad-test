import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";
import { StorageItem } from "./types/storage.type";
import { DEFAULT_INTERVAL, DEFAULT_NUMBER_OF_ARRAYS } from "../constants/app.constants";

const DEFAULT_VALUE: StorageItem = {
  data: [],
  numberOfArrays: DEFAULT_NUMBER_OF_ARRAYS,
  interval: DEFAULT_INTERVAL,
  additionalIds: [],
}

/**
 * Simple storage service, instead of it can be used NgRx, especially if application requires more than one storage
 */
@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private readonly arraySubject: BehaviorSubject<StorageItem> = new BehaviorSubject(DEFAULT_VALUE);
  public readonly storage$: Observable<StorageItem> = this.arraySubject.asObservable();

  /**
   * Set data
   *
   * @param data
   */
  setData(data:Partial<StorageItem>): void {
    this.arraySubject.next({
      ...this.arraySubject.getValue(),
      ...data,
    });
  }
}
