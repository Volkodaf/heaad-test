import DashboardItemDto from "../../../modules/dashboard/dto/dashboard-item.dto";

export type StorageItem = {
  data: DashboardItemDto[];
  numberOfArrays: number;
  interval: number;
  additionalIds: string[];
}
