import * as tsconfig from './tsconfig.json';

import type { Config } from '@jest/types';
import { pathsToModuleNameMapper } from 'ts-jest';

const config: Config.InitialOptions = {
  preset: 'jest-preset-angular',
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: ['<rootDir>/src/test-setup.ts'],
  globalSetup: 'jest-preset-angular/global-setup',
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/dist/'],
  transform: {
    '^.+.(ts|mjs|js|html)$': [
      'jest-preset-angular',
      {
        tsconfig: '<rootDir>/tsconfig.spec.json',
        stringifyContentPathRegex: '\\.(html|svg)$',
        isolatedModules: true,
      },
    ]
  },
  transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
  maxWorkers: 4,
  extensionsToTreatAsEsm: ['.ts'],
  moduleNameMapper: {
    '.*service-worker.util': './mocks/worker.util.mock.ts',
  },

  /* Coverage */
  collectCoverage: false,
  coverageDirectory: 'coverage',
  coverageProvider: 'v8',
  coverageReporters: ['clover', 'json', 'lcov', 'text', 'text-summary', 'cobertura'],
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{service,component,container,pipe,directive,effects,reducer,selectors,util,utils}.{ts,js}',
  ],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: 'coverage',
        outputName: 'junit.xml',
      },
    ],
  ],
};

export default config;
